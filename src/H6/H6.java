package H6;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class H6 {

	public static void main(String[] args) {
		// 常用图片的格式可以通过读取文件头部分获得(16进制)：
		// （1）JPEG
		// - 文件头标识 (2 bytes): 0xff, 0xd8 (SOI) (JPEG 文件标识)
		// （3）PNG
		// - 文件头标识 (8 bytes) 89 50 4E 47 0D 0A 1A 0A
		// （4）GIF
		// - 文件头标识 (6 bytes) 47 49 46 38 39(37) 61
		// （5）BMP
		// - 文件头标识 (2 bytes) 42 4D，字符即： B M
		// 输入一个文件名，判断其真正格式和其文件类型是否匹配（
		// 防止直接给文件改名，比如把一个jpg改成bmp或者把一个rar改成jpg）
		Scanner a = new Scanner(System.in);
		System.out.println("输入一个文件名");
		String s = a.next();
		String ss = s.substring(s.lastIndexOf('.') + 1);
		DataInputStream fi;
		try {
			fi = new DataInputStream(new FileInputStream(s));
			if (ss.equals("JPEG") || ss.equals("jpeg")) {
				if (((fi.readByte()) ^ 0xff) == 0
						& ((fi.readByte()) ^ 0xd8) == 0)
					System.out.println("匹配");
				else
					System.out.println("不匹配");
			} else if (ss.equals("png")) {
				if ((fi.readInt() ^ 0x89504E47) == 0
						& ((fi.readInt() ^ 0x0D0A1A0A) == 0)) {
					System.out.println("类型匹配");
				} else {
					System.out.println("类型不匹配");
				}
			} else if (ss.equals("gif")) {
				if ((fi.readInt() ^ 0x47494638) == 0
						&& (((fi.readByte() ^ 0x39) == 0) || ((fi.readByte() ^ 0x37) == 0))
						&& ((fi.readByte() ^ 0x61) == 0)) {
					System.out.println("类型匹配");
				} else {
					System.out.println("类型不匹配");
				}
			} else if (ss.equals("bmp")) {
				if (((fi.readByte()) ^ 0x42) == 0
						& ((fi.readByte()) ^ 0x4D) == 0) {
					System.out.println("类型匹配");
				} else {
					System.out.println("类型不匹配");
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
