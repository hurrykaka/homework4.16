package H6;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.imageio.stream.FileImageInputStream;

public class ByTeacher {

	public static void main(String[] args) throws Exception {
		File f = new File("/Users/mac/Desktop/5.JPEG");
		FileInputStream fi = new FileInputStream("/Users/mac/Desktop/5.JPEG");
		int[] ar = new int[8];
		for (int i = 0; i < ar.length; i++) {
			ar[i] = fi.read();
		}
		fi.close();

		String name = f.getName();
		String geshi = name.substring(name.lastIndexOf('.') + 1);
		if (geshi.equals("jpeg") || geshi.equals("JPEG")) {
			if (ar[0] == 0xff && ar[1] == 0xd8)
				System.out.println("格式匹配");
			else
				System.out.println("不匹配");
		}
		if (geshi.equals("png") || geshi.equals("PNG")) {
			String[] p = { "89", "50", "4E", "47", "0D", "0A", "1A", "0A" };
			boolean t = true;
			String ss = "";
			for (int i = 0; i < 8; i++) {
				if (!(ss + ar[i]).equals(p[i])) {
					t = false;
					break;
				}
			}
			if (t)
				System.out.println("格式匹配");
			else
				System.out.println("不匹配");
		}

	}
}
