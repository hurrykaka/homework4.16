package H7;

import java.io.Serializable;

public class Book implements Serializable {
	@Override
	public String toString() {
		return "Book [name=" + name + ", writer=" + writer + ", page=" + page
				+ ", time=" + time + "]";
	}

	String name, writer;
	int page, time;

	public Book(String name, String writer, int page, int time) {
		super();
		this.name = name;
		this.writer = writer;
		this.page = page;
		this.time = time;
	}
}
