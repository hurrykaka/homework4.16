package H7;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.imageio.stream.FileImageInputStream;

public class H7 {

	public static void main(String[] args) {
		// 定义Book对象，包括名称，出版时间，页数，作者等，把对象写入文件，并读出
		ObjectOutputStream oo;
		ObjectInputStream oi;
		try {
			oo = new ObjectOutputStream(new FileOutputStream(
					"/Users/mac/Desktop/6.txt"));
			Book b = new Book("红楼春梦", "老赵", 1000, 1970);
			oo.writeObject(b);
			oi = new ObjectInputStream(new FileInputStream(
					"/Users/mac/Desktop/6.txt"));
			Book bo = (Book) oi.readObject();
			System.out.println(bo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
