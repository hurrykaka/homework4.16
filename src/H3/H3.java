package H3;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class H3 {

	public static void main(String[] args) {
		// 3：实现文件剪切的功能，输入一个源文件名，和目标文件夹，剪切后文件名保持不变。
		FileInputStream fi;
		FileOutputStream fo;
		try {
			fi = new FileInputStream("/Users/mac/Desktop/5.txt");
			fo = new FileOutputStream("/Users/mac/Desktop/新建文件夹/5.txt");
			File f = new File("/Users/mac/Desktop/5.txt");
			int l = 0;
			while ((l = fi.read()) != -1) {
				fo.write(l);
				fo.flush();
				f.delete();
			}
			fi.close();
			fo.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
