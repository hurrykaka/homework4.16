package H5;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class H5 {

	public static void main(String[] args) {
		// 5：把 1-10000内的所有质数 写入一个data.txt中，10个一行
		try {
			BufferedWriter bf = new BufferedWriter(new FileWriter(
					"/Users/mac/Desktop/data.txt"));
			bf.write("1");
			bf.write("2");
			bf.write("3");
			int[] a = new int[10000];
			int i = 4;
			int k = 3;
			boolean t = true;
			for (; i <= 10000; i++) {
				t = true;
				String c = " ";
				for (int j = 2; j * j <= i; j++) {
					if (i % j == 0 && i != j) {
						t = false;
						break;
					}
				}
				if (t) {
					c = c + i;
					bf.write(c);
					bf.flush();
					k++;
					if (k % 10 == 0) {
						bf.newLine();
					}
				}
			}

			bf.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
