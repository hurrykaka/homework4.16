package H2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class H2 {

	public static void main(String[] args) {
		// 实现文件复制的功能，通过命令行参数，输入一个源文件名，和目标文件夹，复制后文件名保持不变。
		// 比如 文件名是d:\mypicture\1.jpg,
		// 目标文件夹是 e:\mydoc\，则复制后文件夹e:\mydoc将会出现1.jpg.
		FileReader fr;
		FileWriter fw;
		try {
			fr = new FileReader("/Users/mac/Desktop/1.jpg");
			fw = new FileWriter("/Users/mac/Desktop/新建文件夹/1.jpg");
			char[] a = new char[1];
			int l = 0;
			while ((l = fr.read(a)) != -1) {
				fw.write(new String(a, 0, l));

			}

			fr.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
